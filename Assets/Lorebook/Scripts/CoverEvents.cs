﻿using UnityEngine;
using System.Collections;

public class CoverEvents : MonoBehaviour
{
    /// <summary>
    /// A refrence to bookcontroller model
    /// </summary>
    public BookController bookController;
    
    /// <summary>
    /// Cover event on OnMouseEnter 
    /// </summary>
	void OnMouseEnter()
    {
		bookController.startOpening();
	}


    /// <summary>
    /// Cover event on OnMouseOver 
    /// </summary>
	void OnMouseOver()
    {
		bookController.startOpening();
	}


    /// <summary>
    /// Cover event on OnMouseExit 
    /// </summary>
    void OnMouseExit()
    {
		bookController.stopOpening();
	}


    /// <summary>
    ///  Cover event on OnMouseDown 
    /// </summary>
	void OnMouseDown ()
    {
		bookController.openBook();
	}
    
}
