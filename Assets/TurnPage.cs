﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum PageSide
{
Left,
Right
}

public class TurnPage : MonoBehaviour, IInputClickHandler, IFocusable{

    [SerializeField]
    private BookController m_bookController;
    [SerializeField]
    private PageSide m_pageSide;

    private void TurnBookPage()
    {
        switch (m_pageSide)
        {
            case PageSide.Left: m_bookController.prevPage();
                break;
            case PageSide.Right: m_bookController.nextPage();
                break;
        }
    }


    public void OnInputClicked(InputEventData eventData)
    {
        TurnBookPage();
    }

    public void OnFocusEnter()
    {
        //throw new NotImplementedException();
    }

    public void OnFocusExit()
    {
        TurnBookPage();
    }
}
