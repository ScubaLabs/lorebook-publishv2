﻿using UnityEngine;

public class LeftButtonCommands : MonoBehaviour
{
    public GameObject bookController;
    
    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture
    /// </summary>
    void OnSelect()
    {
        Debug.Log("left button OnSelect called");
        bookController.GetComponent<BookController>().prevPage();
    }

}