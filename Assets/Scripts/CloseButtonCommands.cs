﻿using UnityEngine;
public class CloseButtonCommands : MonoBehaviour
{
    /// <summary>
    /// Called by GazeGestureManager when the user performs a Select gesture.
    /// </summary>
    public void OnSelect()
    {

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            // EditorApplication.Exit(0);           
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }

        else
        {
            Application.Quit();
        }

    }
}